Ultra Power Mega Super Rouguelike
============



1st Year of Computer Engineering Project

Ultra Power Mega Super Rouguelike is a browser game developed in C ready
to be installed in a linux web-server.



Install and Play
-------------



1 - in terminal change directory to /Rouguelike/src

2 - execute "Make"

3 - start apache service

4 - go to http://localhost/cgi-bin/user

5 - start playing



Credits
-------

* Catarina Machado - a81047@alunos.uminho.pt
* Cecília Soares   - a34900@alunos.uminho.pt
* João Vilaça      - a82339@alunos.uminho.pt
